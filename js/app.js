Vue.use(window.vuelidate.default);
Vue.use(VueMask.VueMaskPlugin);
const { required, numeric, email, minValue, helpers, } = window.validators;

const cuit = function (cuit) {
    const cuitRegex = /[0-9]{2}-[0-9]{8}-[0-9]{1}/g;
    if (!cuitRegex.test(cuit))
        return false;
    const [type, value, verificationDigit] = cuit.split('-');
    const numbers = (type + value).split('');
    const numericDigits = [5, 4, 3, 2, 7, 6, 5, 4, 3, 2];
    const module = numbers.reduce((acum, num, index) => acum + num * numericDigits[index], 0) % 11;
    const differenceValue = 11 - module;
    const validationNumber = differenceValue === 11 ? 0 : differenceValue;
    return validationNumber === 10 || validationNumber !== +verificationDigit ? false : true;
};

const phone = helpers.regex("phone", /(\+54)?[\s-]?([0-9]{1,2})?[\s-]?([0-9]{1,4})?[\s-]?[0-9]{4}[\s-]?[0-9]{4}/g);

var app = new Vue({
    el: "#contact",
    components: {
        'vue-recaptcha': VueRecaptcha
    },
    data: {
        context: "",
        btnMsg: "Enviar",
        sent: false,
        sentError: false,
        loading: false,
        recaptcha: "",
        form: {
            companyName: "",
            cuit: "",
            contactName: "",
            contactRole: "",
            contactEmail: "",
            contactPhone: "",
            fleetQuantity: "",
            hasYpfRuta: "No",
            selectedPlan: "",
            comments: "",
            provincia: ""
        },
    },
    mounted: function () {
        var self = this;
        self.context = window.siteContext;
    },
    validations: {
        recaptcha: {
            required
        },
        form: {
            companyName: {},
            cuit: {
                required,
                cuit,
            },
            contactName: {
                required
            },
            contactRole: {},
            contactEmail: {
                required,
                email,
            },
            contactPhone: {
                required,
                numeric,
            },
            fleetQuantity: {
                required,
                numeric,
                minValue: minValue(1),
            },
            hasYpfRuta: {},
            selectedPlan: {},
            comments: {},
            provincia: { required }
        },
    },
    methods: {
        onCaptchaVerified: function (recaptchaToken) {
            const self = this;
            self.recaptcha = recaptchaToken;
        },
        onCaptchaExpired: function () {
            self.recaptcha = "";
            this.$refs.recaptcha.reset();
        },
        sendForm: function (e) {
            e.preventDefault();
            var self = this;
            self.$v.$touch();
            if (!self.$v.$invalid) {
                self.loading = true;
                self.btnMsg = "Enviando...";
                self.sentError = false;
                var webMethod = self.context + "/_layouts/YForms/ServiceV2.aspx/saveOpenItem";
                axios.post(webMethod, {
                    context: self.context,
                    formName: "contacto",
                    title: "",
                    form: JSON.stringify(self.form),
                    email: self.form.contactEmail,
                }, {
                    "Content-type": "application/json; charset=utf-8",
                }).then(function (response) {
                    var r = self.isJsonString(response.data.d) ? JSON.parse(response.data.d) : {
                        Message: "response error",
                    };
                    if (typeof r['Message'] != "undefined") {
                        self.sentError = true;
                        self.scrolling("error-icon");
                    } else {
                        self.sent = true;
                        self.form = {
                            companyName: "",
                            cuit: "",
                            contactName: "",
                            contactRole: "",
                            contactEmail: "",
                            contactPhone: "",
                            fleetQuantity: "",
                            hasYpfRuta: "No",
                            selectedPlan: "",
                            comments: "",
                            provincia: ""
                        };
                        !self.$v.$reset();
                        self.scrolling("success-icon");
                    }
                    self.loading = false;
                    self.btnMsg = "Enviar";
                }).catch(function (error) {
                    self.sentError = true;
                    self.loading = false;
                    self.btnMsg = "Enviar";
                    self.scrolling("error-icon");
                });
            } else {
                for (var key in Object.keys(self.$v.form)) {
                    const input = Object.keys(self.$v.form)[key];
                    if (self.$v.form[input].$error) {
                        self.scrolling(input);
                        break;
                    }
                }
            }
        },
        toggle: function () {
            var self = this;
            if (self.form.hasYpfRuta == "No" || self.form.hasYpfRuta == "") {
                self.form.hasYpfRuta = "Si";
            } else {
                self.form.hasYpfRuta = "No";
            }
        },
        scrolling: function (name) {
            var elm = document.getElementById(name);
            if (elm != null && typeof elm.scrollIntoView != "undefined") {
                elm.scrollIntoView();
            }
            window.scroll(0, window.scrollY - 150);
        },
        isJsonString: function(str) {
            try {
                JSON.parse(str);
            } catch (e) {
                return false;
            }
            return true;
        },
    },
});