function selectTab() {

}

function initializeTabs(tabsWrapper) {
  const tabList = tabsWrapper.querySelectorAll('.tab');
  const tabHeaderList = tabsWrapper.querySelectorAll('.header__item');
  const tabHeaderListElement = tabsWrapper.querySelector('.header__item-list');
  let selectedTabDisplay = null;

  function selectTab({ target }) {
    selectedTabDisplay.textContent = target.textContent;

    tabList.forEach((tab, index) => {
      if (tab.dataset.tabIndex === target.dataset.tabIndex) {
        tabHeaderList[index].classList.add('header__item--active');
        tab.classList.add('tab--active');
      } else {
        tabHeaderList[index].classList.remove('header__item--active');
        tab.classList.remove('tab--active');
      }
    })
  }

  function toggleList({ target }) {
    tabHeaderListElement.parentElement.classList.toggle('tabs__header--open');
  }

  if (tabList && tabHeaderList && tabList.length === tabHeaderList.length) {
    const tabHeaderWrapper = tabHeaderListElement.parentElement;
    selectedTabDisplay = tabHeaderList[0].cloneNode(true);

    selectedTabDisplay.classList.add('header__item--selection');
    selectedTabDisplay.addEventListener('click', toggleList);
    tabHeaderWrapper.append(selectedTabDisplay);

    tabHeaderList.forEach((tabHeader, index) => {
      tabHeader.dataset.tabIndex = index;
      tabList[index].dataset.tabIndex = index;

      tabHeader.addEventListener('click', selectTab);

      if (index === 0) {
        tabHeader.classList.add('header__item--active');
        tabList[index].classList.add('tab--active');
      }
    });
  }
}

function initializeProductsAndServicesPage() {
  const tabsWrapperList = document.querySelectorAll('.tabs');

  tabsWrapperList.forEach(initializeTabs);
}

initializeProductsAndServicesPage();
