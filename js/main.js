const DARK_BG_OFFSET = 100;

function initializeFooter() {
  const buttons = document.querySelectorAll("#footer button");

  const handleClick = ({ currentTarget }) =>
    buttons.forEach((button) => {
      const list = document.querySelector(`#footer ul[data-index="${button.dataset.index}"]`);
      if (button.dataset.index === currentTarget.dataset.index) {
        list.classList.toggle('active');
        button.classList.toggle('active');
      } else {
        list.classList.remove('active');
        button.classList.remove('active');
      }
    });

  buttons.forEach((button) => button.addEventListener("click", handleClick));
}

initializeFooter();

// Navbar

function initializeNav() {
  const nav = document.querySelector('.nav');
  const navMenuToggle = nav.querySelector('.nav__menu-toggle');
  const isHome = location.pathname === '/' || location.pathname === '/index.html';

  function toggleNav() {
    nav.classList.toggle('nav--active');
  }

  function handleScroll() {
    if (window.scrollY > DARK_BG_OFFSET) {
      nav.classList.add('nav--scrolled');
    } else {
      nav.classList.remove('nav--scrolled');
    }
  }

  if (!isHome) {
    nav.classList.add('nav--dark');
  } else {
    window.addEventListener('scroll', handleScroll);
  }

  navMenuToggle.addEventListener('click', toggleNav);
}

initializeNav();

function initializeContact() {
  const switchBtn = document.querySelector('.form__switch-input');
  switchBtn.addEventListener('click', () => {
    switchBtn.parentElement.classList.toggle('active');
  });
}

initializeContact();

function initContactForm() {
  const form = document.getElementById('contact-form');
  const submitForm = form.querySelector('[type="submit"]');
  submitForm.addEventListener('click', validateForm);

  const cuit = form.querySelector('[name="cuit"]');
  const cuitErrorMsg = form.querySelector('.field-input__error-msg[data-type="cuit"]');
  cuit.addEventListener('keypress', () => cuitErrorMsg.classList.remove('active'));

  const email = form.querySelector('[name="contact-email"]');
  const emailErrorMsg = form.querySelector('.field-input__error-msg[data-type="email"]');
  email.addEventListener('keypress', () => emailErrorMsg.classList.remove('active'));

  const phoneNumber = form.querySelector('[name="contact-phone"]');
  const phoneErrorMsg = form.querySelector('.field-input__error-msg[data-type="phone"]');
  phoneNumber.addEventListener('keypress', () => phoneErrorMsg.classList.remove('active'));
}

initContactForm();

function validateCUIT(cuit) {
  const cuitRegex = /[0-9]{2}-[0-9]{8}-[0-9]{1}/g;
  if (!cuitRegex.test(cuit)) return false;
  const [type, value, verificationDigit] = cuit.split('-');
  const numbers = (type + value).split('');
  const numericDigits = [5, 4, 3, 2, 7, 6, 5, 4, 3, 2];
  const module = numbers.reduce((acum, num, index) => acum + num * numericDigits[index], 0) % 11;
  const differenceValue = 11 - module;
  const validationNumber = differenceValue === 11 ? 0 : differenceValue;
  return validationNumber === 10 || validationNumber !== +verificationDigit ? false : true;
}

function validateEmail(email) {
  const emailRegex = /^\S+@\S+$/g;
  return emailRegex.test(email);
}

function validatePhone(phone) {
  const phoneRegex = /(\+54)?[\s-]?([0-9]{1,2})?[\s-]?([0-9]{1,4})?[\s-]?[0-9]{4}[\s-]?[0-9]{4}/g;
  return phoneRegex.test(phone);
}

function validateForm(event) {
  event.preventDefault();

  const form = document.getElementById('contact-form');
  const cuit = form.querySelector('[name="cuit"]').value;
  const email = form.querySelector('[name="contact-email"]').value;
  const phoneNumber = form.querySelector('[name="contact-phone"]').value;
  let error;

  if (!validateCUIT(cuit)) {
    const errorMsg = form.querySelector('.field-input__error-msg[data-type="cuit"]');
    errorMsg.classList.add('active');
    error = true;
  }

  if (!validateEmail(email)) {
    const errorMsg = form.querySelector('.field-input__error-msg[data-type="email"]');
    errorMsg.classList.add('active');
    error = true;
  }

  if (!validatePhone(phoneNumber)) {
    const errorMsg = form.querySelector('.field-input__error-msg[data-type="phone"]');
    errorMsg.classList.add('active');
    error = true;
  }

  if(!error) {
    try { // request
      const successMsg = form.querySelector('.form-msg[data-type="success"]');
      successMsg.classList.add('active');
      console.log('success')
    } catch(e) {
      const errorMsg = form.querySelector('.form-msg[data-type="error"]');
      errorMsg.classList.add('active');
      console.log('success')
    }
  }
}
