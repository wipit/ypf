let planCardList = false;

function initializePlansPage() {
  planCardList = document.querySelectorAll('.plan-card');

  planCardList.forEach(planCard => {
    const expanderButton = planCard.querySelector('.plan-card-expander__button');

    expanderButton.addEventListener('click', event => {
      planCard.classList.toggle('plan-card--expanded');
    });
  });
}

initializePlansPage();

/* COPIADO DE LA HOME */
let carouselInitialized = false;
let plansList = null;

const mutationObserver = new MutationObserver(handleMutation);

function handleMutation(mutations) {
  mutations.forEach(mutationRecord => {
    if (mutationRecord.target.classList.contains('carousel')) {
      window.initializeCarousel({ root: mutationRecord.target });
    } else {
      window.destroyCarousel({ root: mutationRecord.target });
    }
  })
}

function initializeHomePage() {
  plansList = document.querySelector('.plans-list');
  mutationObserver.observe(plansList, { attributes: true });
  window.initializeCarousel({ root: plansList });
}

initializeHomePage();

