let carouselInitialized = false;
let benefitsList = null;
let cardsList = null;

const mutationObserver = new MutationObserver(handleMutation);

function handleMutation(mutations) {
  mutations.forEach(mutationRecord => {
    if (mutationRecord.target.classList.contains('carousel')) {
      window.initializeCarousel({ root: mutationRecord.target });
    } else {
      window.destroyCarousel({ root: mutationRecord.target });
    }
  })
}

function initializeArticlePage() {
  cardsList = document.querySelector('.carousel-list-2');
  mutationObserver.observe(cardsList, { attributes: true });
  window.initializeCarousel({ root: cardsList });
  
  benefitsList = document.querySelector('.carousel-list');
  mutationObserver.observe(benefitsList, { attributes: true });
  window.initializeCarousel({ root: benefitsList });
}

setTimeout(initializeArticlePage, 2000);

function scrollToTop() {
  window.scrollTo({top: 0, behavior: 'smooth'});
}