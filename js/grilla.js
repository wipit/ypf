function GridVue(ClientID) {
  /*------------- VUE APP ----------------*/

  var app = new Vue({
    el: ClientID,
    data: {
      formName: "",
      finalizedFlag: false,
      siteUrl: "",
      items: [],
      fields: [],
      loadingMsg: "",
      loading: false,
    },
    mounted: function () {
      var self = this;
      self.formTitle = self.getParameter("t");
      self.finalizedFlag = self.getParameter("f") == 1 ? true : false;
      self.siteUrl =
        window.location.protocol +
        "//" +
        window.location.host +
        _spPageContextInfo.webServerRelativeUrl;
      self.$nextTick(function () {
        self.getDataToForm(0);
      });
    },
    methods: {
      getDataToForm: function (index) {
        var self = this;
        self.loadingMsg = "Obteniendo Items.";
        self.loading = true;
        var webMethod = "/_layouts/YForms/ServiceV1.aspx/getAllItems";
        axios
          .post(
            self.siteUrl + webMethod,
            {
              formName: self.formTitle,
              context: self.siteUrl,
              initIndex: index,
              onlyFinalized: self.finalizedFlag,
            },
            {
              "Content-type": "application/json; charset=utf-8",
            }
          )
          .then(function (response) {
            var response = JSON.parse(response.data.d);
            if (self.fields.length == 0) {
              self.fields = JSON.parse(response.fields);
              self.fields.unshift({
                type: "number",
                visible: false,
                title: "Index",
                name: "index",
              });
              self.fields.unshift({
                type: "text",
                visible: false,
                title: "Creado",
                name: "create",
              });
            }
            for (var k = 0; k < response.items.length; k++) {
              var nObj = JSON.parse(response.items[k].data);
              nObj.index = response.items[k].index;
              nObj.create = response.items[k].create;
              self.items.push(nObj);
            }
            if (response.lastIndex > 0) {
              self.getDataToForm(response.lastIndex);
            } else {
              self.configGrid();
              self.loadingMsg = "";
              self.loading = false;
            }
          });
      },
      configGrid: function () {
        var self = this;
        jsGrid.locale("es");
        self.gridObj = $("#jsGrid").jsGrid({
          width: "100%",
          height: "auto",
          sorting: true,
          paging: true,
          autoload: false,
          pageSize: 100,
          pageButtonCount: 5,
          data: self.items,
          fields: self.fields,
        });
        $("input[class='toppings']").click(function () {
          if ($(this).prop("checked"))
            $("#jsGrid").jsGrid("fieldOption", $(this).val(), "visible", true);
          else
            $("#jsGrid").jsGrid("fieldOption", $(this).val(), "visible", false);
        });
      },
      refreshGrid: function () {
        $("#jsGrid").jsGrid("refresh");
      },
      exportToExcel: function () {
        var self = this;
        var headers = [];
        var titles = {};
        for (var i = 0; i < self.fields.length; i++) {
          var field = self.fields[i];
          headers.push(field.name);
          titles[field.name] = field.title;
        }

        var fileName = "datos_" + self.formTitle + ".xlsx";

        var ws = XLSX.utils.json_to_sheet(self.items, {
          header: headers,
        });

        const range = XLSX.utils.decode_range(ws["!ref"]);

        for (var c = range.s.c; c <= range.e.c; c++) {
          const header = XLSX.utils.encode_col(c) + "1";
          ws[header].v = titles[ws[header].v];
        }

        var wb = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, self.formTitle);

        XLSX.writeFile(wb, fileName);
      },
      getParameter: function (name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
          results = regex.exec(location.search);
        return results === null
          ? ""
          : decodeURIComponent(results[1].replace(/\+/g, " "));
      },
    },
  });
  /*---------- FIN VUE APP ----------------*/
}
