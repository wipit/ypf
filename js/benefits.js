
// Benefits Section

function initializeBenefitsSection() {
  const carouselNode = document.querySelector('.benefits-carousel__wrapper');
  const carouselSlides = carouselNode.querySelectorAll('.benefits-card');
  const carouselPreviousButton = document.querySelector('.benefits-carousel [data-control="previous"]')
  const carouselNextButton = document.querySelector('.benefits-carousel [data-control="next"]')
  const carousel = window.EmblaCarousel(carouselNode, {
    loop: true,
    draggableClass: 'is-draggable',
    draggingClass: 'is-dragging'
  });

  carouselSlides.forEach((slide, index) => slide.dataset.index = index);

  function handleCarouselInit() {
    carouselSlides[0].classList.add('benefits-card--active');
  }

  function handleSlideSelection() {
    const previousIndex = carousel.previousScrollSnap();
    const selectedIndex = carousel.selectedScrollSnap();
    const selectedSlide = carouselNode.querySelector(`[data-index="${selectedIndex}"]`);
    const previousSlide = carouselNode.querySelector(`[data-index="${previousIndex}"]`);

    previousSlide.classList.remove('benefits-card--active');
    selectedSlide.classList.add('benefits-card--active');
  }

  carouselPreviousButton.addEventListener('click', carousel.scrollPrev);
  carouselNextButton.addEventListener('click', carousel.scrollNext);

  carousel.on('init', handleCarouselInit);
  carousel.on('select', handleSlideSelection);
}

initializeBenefitsSection();
