let planCardList = false;

function initializePlansPage() {
  planCardList = document.querySelectorAll('.plan-card');

  planCardList.forEach(planCard => {
    const expanderButton = planCard.querySelector('.plan-card-expander__button');

    expanderButton.addEventListener('click', event => {
      planCard.classList.toggle('plan-card--expanded');
    });
  });
}

initializePlansPage();
