let carouselInitialized = false;
let advancedFeaturesList = null;

const mutationObserver = new MutationObserver(handleMutation);

function handleMutation(mutations) {
  mutations.forEach(mutationRecord => {
    if (mutationRecord.target.classList.contains('carousel')) {
      window.initializeCarousel({ root: mutationRecord.target });
    } else {
      window.destroyCarousel({ root: mutationRecord.target });
    }
  })
}

function handleResize() {
  if (window.innerWidth < 760 && !carouselInitialized) {
    advancedFeaturesList.classList.add('carousel');
    carouselInitialized = true;
  } else if (window.innerWidth >= 760 && carouselInitialized) {
    advancedFeaturesList.classList.remove('carousel');
    carouselInitialized = false;
  }
}

function initializeAnalyticsPage() {
  advancedFeaturesList = document.querySelector('.advanced-features__list');
  mutationObserver.observe(advancedFeaturesList, { attributes: true });

  handleResize();

  window.addEventListener('resize', handleResize);
}

initializeAnalyticsPage();
