let carouselInitialized = false;
let benefitsList = null;
let plansList = null;
let newsList = null;

const mutationObserver = new MutationObserver(handleMutation);

function handleMutation(mutations) {
  mutations.forEach(mutationRecord => {
    if (mutationRecord.target.classList.contains('carousel')) {
      window.initializeCarousel({ root: mutationRecord.target });
    } else {
      window.destroyCarousel({ root: mutationRecord.target });
    }
  })
}

function initializeHomePage() {
  benefitsList = document.querySelector('.benefits-list');
  mutationObserver.observe(benefitsList, { attributes: true });
  window.initializeCarousel({ root: benefitsList });

  plansList = document.querySelector('.plans-list');
  mutationObserver.observe(plansList, { attributes: true });
  window.initializeCarousel({ root: plansList });

  newsList = document.querySelector('.news-list');
  mutationObserver.observe(newsList, { attributes: true });
  window.initializeCarousel({ root: newsList });

}

setTimeout(initializeHomePage, 2000);