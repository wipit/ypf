function selectTab() {

}

function initializeFaqsPage() {
  const questionsList = document.querySelectorAll('.question');

  function expandQuestion({ target }) {
    const question = target.parentElement;
    question.classList.toggle('question--collapsed');
  }

  function toggleList({ target }) {
    tabHeaderListElement.parentElement.classList.toggle('tabs__header--open');
  }

  if (questionsList && questionsList.length) {
    questionsList.forEach((question, index) => {
      const questionTitleElement = question.querySelector('.question__title');

      question.classList.add('question--collapsed');

      questionTitleElement.addEventListener('click', expandQuestion);
    });
  }
}

initializeFaqsPage();
