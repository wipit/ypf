const DEFAULT_CAROUSEL_OPTIONS = {
  loop: true,
  draggableClass: "is-draggable",
  draggingClass: "is-dragging",
};

function buildCarouselControls() {
  const carouselControls = document.createElement("div");
  const carouselNextButton = document.createElement("button");
  const carouselPrevButton = document.createElement("button");
  const nextButtonInnerHTML = `
    <svg width="24" height="24" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M19.75 11.7257h-15M13.7002 5.70131l6.05 6.02399-6.05 6.025"
        stroke="currentColor"
        stroke-width="1.5"
        stroke-linecap="round"
        stroke-linejoin="round"
      ></path>
    </svg>
  `;
  const prevButtonInnerHTML = `
    <svg width="24" height="24" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M4.25 12.2743h15M10.2998 18.2987l-6.05-6.024 6.05-6.02501"
        stroke="currentColor"
        stroke-width="1.5"
        stroke-linecap="round"
        stroke-linejoin="round"
      ></path>
    </svg>
  `;

  carouselPrevButton.classList.add("carousel__control");
  carouselPrevButton.classList.add("carousel__control--prev");
  carouselPrevButton.dataset.control = "prev";
  carouselPrevButton.innerHTML = prevButtonInnerHTML;

  carouselNextButton.classList.add("carousel__control");
  carouselNextButton.classList.add("carousel__control--next");
  carouselNextButton.dataset.control = "next";
  carouselNextButton.innerHTML = nextButtonInnerHTML;

  carouselControls.classList.add("carousel__controls");
  carouselControls.appendChild(carouselPrevButton);
  carouselControls.appendChild(carouselNextButton);

  return {
    controls: carouselControls,
    previousButton: carouselPrevButton,
    nextButton: carouselNextButton
  };
}

function initializeCarousel({ root, slides: baseSlides } = {}, options = DEFAULT_CAROUSEL_OPTIONS) {
  if (!root) {
    return;
  }

  const { controls, previousButton, nextButton } = buildCarouselControls();
  const slides = baseSlides && baseSlides.length ? baseSlides : Array.from(root.children);

  const carouselViewport = document.createElement("div");
  carouselViewport.classList.add("carousel__viewport");

  const carouselSlidesWrapper = document.createElement("div");
  carouselSlidesWrapper.classList.add("carousel__slides-wrapper");

  slides.forEach((slide, index) => {
    const slideWrapper = document.createElement("div");
    slideWrapper.dataset.index = index;
    slideWrapper.classList.add("carousel__slide");

    slideWrapper.appendChild(slide);
    carouselSlidesWrapper.appendChild(slideWrapper);
  });

  root.innerHTML = "";
  carouselViewport.appendChild(carouselSlidesWrapper);
  root.appendChild(carouselViewport);

  const carousel = window.EmblaCarousel(carouselViewport, options);

  if (previousButton || nextButton) {
    if (nextButton) {
      nextButton.addEventListener("click", carousel.scrollNext);
    }
    if (previousButton) {
      previousButton.addEventListener("click", carousel.scrollPrev);
    }

    root.appendChild(controls);
  }


  function handleCarouselInit() {
    slides[0].parentElement.classList.add("carousel__slide--active");
  }

  function handleSlideSelection() {
    const previousIndex = carousel.previousScrollSnap();
    const selectedIndex = carousel.selectedScrollSnap();
    const selectedSlide = root.querySelector(`[data-index="${selectedIndex}"]`);
    const previousSlide = root.querySelector(`[data-index="${previousIndex}"]`);

    previousSlide.classList.remove("carousel__slide--active");
    selectedSlide.classList.add("carousel__slide--active");
  }

  carousel.on("init", handleCarouselInit);
  carousel.on("select", handleSlideSelection);
}

function destroyCarousel({ root } = {}) {
  if (!root) {
    return;
  }

  const carouselViewport = root.querySelector(".carousel__viewport");
  const carouselControls = root.querySelector(".carousel__controls");
  const carouselSlidesWrapper = root.querySelector(".carousel__slides-wrapper");

  if (root.classList.contains("carousel")) {
    root.classList.remove("carousel");
  }

  const slides = Array.from(root.querySelectorAll(".carousel__slide"));

  slides.forEach((slide) => {
    root.appendChild(slide.childNodes[0]);
    carouselSlidesWrapper.removeChild(slide);
  });

  root.removeChild(carouselControls);
  root.removeChild(carouselViewport);
}

window.initializeCarousel = initializeCarousel;
