function GridVue(ClientID) {
    /*------------- VUE APP ----------------*/
    var app = new Vue({
        el: ClientID,
        data: {
            siteUrl: window.location.href.split("/Paginas")[0],
            webMethodUrl: "/_layouts/YForms/ServiceV2.aspx/",
            formName: "",
            finalizedFlag: false,
            siteUrl: "",
            items: [],
            fields: [],
            loadingMsg: "",
            error: "",
            loading: false,
        },
        mounted: function () {
            var self = this;
            self.formTitle = self.getParameter("t");
            self.finalizedFlag = self.getParameter("f") == 1 ? true : false;
            if (typeof _spPageContextInfo.webServerRelativeUrl != 'undefined') {
                self.siteUrl = window.location.protocol + "//" + window.location.host + _spPageContextInfo.webServerRelativeUrl;
            }
            self.getFieldsForm();
            self.$nextTick(function () {
                self.getDataToForm(0);
            });
        },
        methods: {
            clean: function () {
                var self = this;
                self.error = "";
                self.loadingMsg = "";
                self.loading = false;
            },
            getFieldsForm: function () {
                var self = this;
                self.loadingMsg = "Obteniendo Campos.";
                self.loading = true;
                axios.post(self.siteUrl + self.webMethodUrl + "getFields", {
                    context: self.siteUrl,
                    formName: self.formTitle,
                }, {
                    "Content-type": "application/json; charset=utf-8",
                }).then(function (response) {
                    var r = isJsonString(response.data.d) ? JSON.parse(response.data.d) : {
                        Message: "response error",
                    };
                    if (typeof r['Message'] != "undefined") {
                        self.error = r.Message;
                    } else {
                        if (typeof r.fields == "object") {
                            for (let i = 0; i < r.fields.length; i++) {
                                const elm = r.fields[i];
                                if (elm.type === "flag") {
                                    elm.itemTemplate = function (value, e) {
                                        var content = document.createElement('div');
                                        content.className = "grid-flag";
                                        var yS = document.createElement('span');
                                        var yI = document.createElement('input');
                                        yI.onclick = function () {
                                            self.setExtraData(e.id, e.Title, elm.name, "Si")
                                        }
                                        yI.type = 'radio';
                                        yI.checked = value == "Si";
                                        yI.id = 'flag_y' + e.id;
                                        yI.name = 'flag_' + e.id;
                                        var yL = document.createElement('label');
                                        yL.htmlFor = 'flag_y' + e.id;
                                        yL.innerText = "Si"
                                        yS.appendChild(yI);
                                        yS.appendChild(yL);
                                        var nS = document.createElement('span');
                                        var nI = document.createElement('input');
                                        nI.onclick = function () {
                                            self.setExtraData(e.id, e.Title, elm.name, "No")
                                        }
                                        nI.type = 'radio';
                                        nI.checked = value == "No";
                                        nI.id = 'flag_n' + e.id;
                                        nI.name = 'flag_' + e.id;
                                        var nL = document.createElement('label');
                                        nL.htmlFor = 'flag_n' + e.id;
                                        nL.innerText = "No"
                                        nS.appendChild(nI);
                                        nS.appendChild(nL);
                                        content.appendChild(yS);
                                        content.appendChild(nS);
                                        return content;
                                    }
                                } else if (elm.type === "date") {
                                    elm.itemTemplate = function (value) {
                                        return '<span>' + self.getDateString(value) + '</span>';
                                    }
                                } else if (elm.type === "link") {
                                    elm.type = "text";
                                    //self.filesFields.push(elm.name);
                                    //TODO: cambiar a array de items en un futuro
                                    elm.itemTemplate = function (value) {
                                        return value.length > 0 ? '<a target="_blank" href="' + self.siteUrl + '/' + value[0].url + '">' + value[0].name + '</a>' : "";
                                    }
                                } else if (elm.type === "text") {
                                    elm.itemTemplate = function (value) {
                                        return value != '' ? self.htmlEntities(value) : '';
                                    }
                                }
                                self.fields = r.fields;
                            }
                        }
                    }
                    self.clean();
                }).catch(function (error) {
                    self.clean();
                    self.error = JSON.parse(error.request.response).Message;
                });
            },
            htmlEntities: function (str) {
                return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
            },
            getDateString: function (str) {
                return str;
            },
            setExtraData: function (id, title, colum, value) {
                var self = this;
                self.loadingMsg = "Guardando cambio.";
                self.loading = true;
                //(string context, string formName, string title, int id, string column, string value)
                axios.post(self.siteUrl + self.webMethodUrl + "setCustomValue", {
                    context: self.siteUrl,
                    formName: self.formTitle,
                    title: title,
                    id: id,
                    column: colum,
                    value: value,
                }, {
                    "Content-type": "application/json; charset=utf-8",
                }).then(function (response) {
                    var r = isJsonString(response.data.d) ? JSON.parse(response.data.d) : {
                        Message: "response error",
                    };
                    if (typeof r['Message'] != "undefined") {
                        self.clean();
                        self.error = r.Message;
                    } else {
                        self.clean();
                    }
                }).catch(function (error) {
                    self.clean();
                    self.error = JSON.parse(error.request.response).Message;
                });
            },
            getDataToForm: function (index) {
                var self = this;
                self.loadingMsg = "Obteniendo Items.";
                self.loading = true;
                axios.post(self.siteUrl + self.webMethodUrl + "getAllItems", {
                    context: self.siteUrl,
                    formName: self.formTitle,
                    initIndex: index,
                    onlyClosed: self.finalizedFlag,
                }, {
                    "Content-type": "application/json; charset=utf-8",
                }).then(function (response) {
                    var r = isJsonString(response.data.d) ? JSON.parse(response.data.d) : {
                        Message: "response error",
                    };
                    if (typeof r['Message'] != "undefined") {
                        self.clean();
                        self.error = r.Message;
                    } else {
                        if (typeof r.items == "object") {
                            for (let i = 0; i < r.items.length; i++) {
                                const elm = r.items[i];
                                var extraData = JSON.parse(elm.extraData);
                                var keys = Object.keys(extraData);
                                var nObj = JSON.parse(elm.data);
                                for (var j = 0; j < keys.length; j++) {
                                    nObj[keys[j]] = extraData[keys[j]]
                                }
                                nObj.status = elm.status;
                                nObj.id = elm.id;
                                nObj.list = elm.list;
                                nObj.modified = elm.modified;
                                nObj.created = elm.created;
                                self.items.push(nObj);
                            }
                        }
                        if (r.lastIndex > 0) {
                            self.getDataToForm(r.lastIndex);
                        } else {
                            self.clean();
                            self.configGrid();
                        }
                    }
                });
            },
            configGrid: function () {
                var self = this;
                jsGrid.locale("es");
                self.gridObj = $("#jsGrid").jsGrid({
                    width: "100%",
                    height: "auto",
                    sorting: true,
                    paging: true,
                    autoload: false,
                    pageSize: 100,
                    pageButtonCount: 5,
                    data: self.items,
                    fields: self.fields,
                });
                $("input[class='toppings']").click(function () {
                    if ($(this).prop("checked"))
                        $("#jsGrid").jsGrid("fieldOption", $(this).val(), "visible", true);
                    else
                        $("#jsGrid").jsGrid("fieldOption", $(this).val(), "visible", false);
                });
            },
            refreshGrid: function () {
                $("#jsGrid").jsGrid("refresh");
            },
            exportToExcel: function () {
                var self = this;
                var headers = [];
                var titles = {};
                for (var i = 0; i < self.fields.length; i++) {
                    var field = self.fields[i];
                    headers.push(field.name);
                    titles[field.name] = field.title;
                }
                var fileName = "datos_" + self.formTitle + ".xlsx";
                var ws = XLSX.utils.json_to_sheet(self.items, {
                    header: headers,
                });
                const range = XLSX.utils.decode_range(ws["!ref"]);
                for (var c = range.s.c; c <= range.e.c; c++) {
                    const header = XLSX.utils.encode_col(c) + "1";
                    ws[header].v = titles[ws[header].v];
                }
                var wb = XLSX.utils.book_new();
                XLSX.utils.book_append_sheet(wb, ws, self.formTitle);
                XLSX.writeFile(wb, fileName);
            },
            getParameter: function (name) {
                name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
                var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                    results = regex.exec(location.search);
                return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
            },
        },
    });

    function isJsonString(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }

    /*---------- FIN VUE APP ----------------*/
}