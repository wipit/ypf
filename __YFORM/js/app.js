var app = new Vue({
    el: "#app",
    data: {
        siteUrl: window.location.href.split("/Paginas")[0],
        webMethodUrl: "/_layouts/YForms/ServiceV2.aspx/",
        form: {
            name: "",
            email: "",
        },
        error: {
            getFormHTML: "",
            getNewTitle: "",
            saveOpenItem: "",
            setCustomValue: "",
            getItem: "",
            saveItem: "",
            saveFile: "",
            getFields: "",
            getAllItems: "",
            itemExists: "",
        },
        success: {
            getFormHTML: "",
            getNewTitle: "",
            saveOpenItem: "",
            setCustomValue: "",
            getItem: "",
            saveItem: "",
            saveFile: "",
            getFields: "",
            getAllItems: "",
            itemExists: "",
        },
        id: "",
        extra: "",
        key: "",
        file: "",
    },
    methods: {
        getFormHTML: function(e) {
            var self = this;
            e.target.classList.add("loading");
            self.error.getFormHTML = "";
            self.success.getFormHTML = "";
            //(string context, string formName)
            axios.post(self.siteUrl + self.webMethodUrl + "getFormHTML", {
                formName: "UnitTest01",
                context: self.siteUrl,
            }, {
                "Content-type": "application/json; charset=utf-8",
            }).then(function(response) {
                var r = isJsonString(response.data.d) ? JSON.parse(response.data.d) : {
                    Message: "response error",
                };
                if (typeof r['Message'] != "undefined") {
                    self.error.getFormHTML = r.Message;
                } else {
                    self.success.getFormHTML = "Llamada completada correctamente.";
                    if (r.form == "") {
                        self.error.getFormHTML = "El formulario no existe.";
                    } else {
                        var p = document.createElement("p");
                        p.textContent = r.form;
                        self.success.getFormHTML += "<hr/> form: " + p.innerHTML;
                    }
                }
                e.target.classList.remove("loading");
            }).catch(function(error) {
                self.error.getFormHTML = JSON.parse(error.request.response).Message;
                e.target.classList.remove("loading");
            });
        },
        getNewTitle: function(e) {
            var self = this;
            e.target.classList.add("loading");
            self.error.getNewTitle = "";
            self.success.getNewTitle = "";
            //(string context, string formName)
            axios.post(self.siteUrl + self.webMethodUrl + "getNewTitle", {
                formName: "UnitTest01",
                context: self.siteUrl,
            }, {
                "Content-type": "application/json; charset=utf-8",
            }).then(function(response) {
                var r = isJsonString(response.data.d) ? JSON.parse(response.data.d) : {
                    Message: "response error",
                };
                if (typeof r['Message'] != "undefined") {
                    self.error.getNewTitle = r.Message;
                } else {
                    self.success.getNewTitle = "Llamada completada correctamente.";
                    if (r.title == "") {
                        self.error.getNewTitle = "El formulario no existe.";
                    } else {
                        self.success.getNewTitle += "<hr/> title: " + r.title;
                    }
                }
                e.target.classList.remove("loading");
            }).catch(function(error) {
                self.error.getNewTitle = JSON.parse(error.request.response).Message;
                e.target.classList.remove("loading");
            });
        },
        saveOpenItem: function(e) {
            var self = this;
            e.target.classList.add("loading");
            self.error.saveOpenItem = "";
            self.success.saveOpenItem = "";
            //(string context, string formName, string title, string form, string email)
            axios.post(self.siteUrl + self.webMethodUrl + "saveOpenItem", {
                context: self.siteUrl,
                formName: "UnitTest01",
                title: self.key,
                form: JSON.stringify(self.form),
                email: self.form.email
            }, {
                "Content-type": "application/json; charset=utf-8",
            }).then(function(response) {
                var r = isJsonString(response.data.d) ? JSON.parse(response.data.d) : {
                    Message: "response error",
                };
                if (typeof r['Message'] != "undefined") {
                    self.error.saveOpenItem = r.Message;
                } else {
                    self.success.saveOpenItem = "Llamada completada correctamente.";
                    if (r.title != "") {
                        self.key = r.title;
                        self.success.saveOpenItem += "<hr/> title: " + r.title;
                    }
                }
                e.target.classList.remove("loading");
            }).catch(function(error) {
                self.error.saveOpenItem = JSON.parse(error.request.response).Message;
                e.target.classList.remove("loading");
            });
        },
        setCustomValue: function(e) {
            var self = this;
            e.target.classList.add("loading");
            self.error.setCustomValue = "";
            self.success.setCustomValue = "";
            //(string context, string formName, string title, int id, string column, string value)
            axios.post(self.siteUrl + self.webMethodUrl + "setCustomValue", {
                context: self.siteUrl,
                formName: "UnitTest01",
                title: self.key,
                id: self.id,
                column: "extra",
                value: self.extra,
            }, {
                "Content-type": "application/json; charset=utf-8",
            }).then(function(response) {
                var r = isJsonString(response.data.d) ? JSON.parse(response.data.d) : {
                    Message: "response error",
                };
                if (typeof r['Message'] != "undefined") {
                    self.error.setCustomValue = r.Message;
                } else {
                    self.success.setCustomValue = "Llamada completada correctamente.";
                    if (r.title != "") {
                        self.key = r.title;
                        self.success.setCustomValue += "<hr/> title: " + r.title;
                    }
                }
                e.target.classList.remove("loading");
            }).catch(function(error) {
                self.error.setCustomValue = JSON.parse(error.request.response).Message;
                e.target.classList.remove("loading");
            });
        },
        getItem: function(e) {
            var self = this;
            e.target.classList.add("loading");
            self.error.getItem = "";
            self.success.getItem = "";
            //(string context, string formName, string title)
            axios.post(self.siteUrl + self.webMethodUrl + "getItem", {
                context: self.siteUrl,
                formName: "UnitTest01",
                title: self.key,
            }, {
                "Content-type": "application/json; charset=utf-8",
            }).then(function(response) {
                var r = isJsonString(response.data.d) ? JSON.parse(response.data.d) : {
                    Message: "response error",
                };
                if (typeof r['Message'] != "undefined") {
                    self.error.getItem = r.Message;
                } else {
                    self.success.getItem = "Llamada completada correctamente.";
                    if (r.id > 0) {
                        self.id = r.id;
                        var dataForm = JSON.parse(r.data);
                        self.form.name = dataForm.name;
                        self.form.email = dataForm.email;
                        var dataExtra = JSON.parse(r.extraData);
                        self.extra = dataExtra.extra;
                        self.success.getItem += "<hr/> id: " + r.id + "<br/> list: " + r.list + "<br/> created: " + r.created + "<br/> modified: " + r.modified + "<br/> data: " + r.data + "<br/> extraData: " + r.extraData + "<br/> status: " + r.status;
                    }
                }
                e.target.classList.remove("loading");
            }).catch(function(error) {
                self.error.getItem = JSON.parse(error.request.response).Message;
                e.target.classList.remove("loading");
            });
        },
        saveItem: function(e) {
            var self = this;
            e.target.classList.add("loading");
            self.error.saveItem = "";
            self.success.saveItem = "";
            //(string context, string formName, string title, string form, string email, string openTitle)
            axios.post(self.siteUrl + self.webMethodUrl + "saveItem", {
                context: self.siteUrl,
                formName: "UnitTest01",
                title: self.form.name,
                form: JSON.stringify(self.form),
                email: self.form.email,
                openTitle: self.key,
            }, {
                "Content-type": "application/json; charset=utf-8",
            }).then(function(response) {
                var r = isJsonString(response.data.d) ? JSON.parse(response.data.d) : {
                    Message: "response error",
                };
                if (typeof r['Message'] != "undefined") {
                    self.error.saveItem = r.Message;
                } else {
                    self.success.saveItem = "Llamada completada correctamente.";
                    if (r.title != "") {
                        self.key = r.title;
                        self.success.saveItem += "<hr/> title: " + r.title;
                    }
                }
                e.target.classList.remove("loading");
            }).catch(function(error) {
                self.error.saveItem = JSON.parse(error.request.response).Message;
                e.target.classList.remove("loading");
            });
        },
        updateFiles: function() {
            var self = this;
            var inputFile = self.$refs.inputFile.files[0];
            self.file = {
                name: inputFile.name,
                url: "",
                file: inputFile,
            };
        },
        saveFile: function(e) {
            var self = this;
            var elm = e;
            e.target.classList.add("loading");
            self.error.saveFile = "";
            self.success.saveFile = "";
            var reader = new FileReader();
            reader._name = self.file.file.name;
            reader.onloadend = function(e) {
                //(string context, string formName, string name, string file, string folder)
                axios.post(self.siteUrl + self.webMethodUrl + "saveFile", {
                    context: self.siteUrl,
                    formName: "UnitTest01",
                    name: reader._name,
                    file: reader.result.split(",").pop(),
                    folder: "adjuntos_" + self.key,
                }, {
                    "Content-type": "application/json; charset=utf-8",
                }).then(function(response) {
                    var r = isJsonString(response.data.d) ? JSON.parse(response.data.d) : {
                        Message: "response error",
                    };
                    if (typeof r['Message'] != "undefined") {
                        self.error.saveFile = r.Message;
                    } else {
                        self.success.saveFile = "Llamada completada correctamente.";
                        if (r.url != "") {
                            self.success.saveFile += "<hr/> url: " + r.url;
                        }
                    }
                    elm.target.classList.remove("loading");
                }).catch(function(error) {
                    self.error.saveFile = JSON.parse(error.request.response).Message;
                    elm.target.classList.remove("loading");
                });
            }
            ;
            reader.readAsDataURL(self.file.file);
        },
        getFields: function(e) {
            var self = this;
            e.target.classList.add("loading");
            self.error.getFields = "";
            self.success.getFields = "";
            self.collFields = [];
            //getFields(string context, string formName)
            axios.post(self.siteUrl + self.webMethodUrl + "getFields", {
                context: self.siteUrl,
                formName: "UnitTest01",
            }, {
                "Content-type": "application/json; charset=utf-8",
            }).then(function(response) {
                var r = isJsonString(response.data.d) ? JSON.parse(response.data.d) : {
                    Message: "response error",
                };
                if (typeof r['Message'] != "undefined") {
                    self.error.getFields = r.Message;
                } else {
                    self.success.getFields = "Llamada completada correctamente.";
                    if (typeof r.fields == "object") {
                        for (let i = 0; i < r.fields.length; i++) {
                            const elm = r.fields[i];
                            self.success.getFields += "<hr/> name: " + elm.name + "<br/> title: " + elm.title + "<br/> visible: " + elm.visible + "<br/> type: " + elm.type;
                        }
                    }
                }
                e.target.classList.remove("loading");
            }).catch(function(error) {
                self.error.getFields = JSON.parse(error.request.response).Message;
                e.target.classList.remove("loading");
            });
        },
        getAllItems: function(e) {
            var self = this;
            e.target.classList.add("loading");
            self.error.getAllItems = "";
            self.success.getAllItems = "";
            self.collItems = [];
            // getAllItems(string context, string formName, int initIndex, bool onlyClosed)
            axios.post(self.siteUrl + self.webMethodUrl + "getAllItems", {
                context: self.siteUrl,
                formName: "UnitTest01",
                initIndex: 0,
                onlyClosed: false,
            }, {
                "Content-type": "application/json; charset=utf-8",
            }).then(function(response) {
                var r = isJsonString(response.data.d) ? JSON.parse(response.data.d) : {
                    Message: "response error",
                };
                if (typeof r['Message'] != "undefined") {
                    self.error.getAllItems = r.Message;
                } else {
                    self.success.getAllItems = "Llamada completada correctamente.";
                    if (typeof r.items == "object") {
                        for (let i = 0; i < r.items.length; i++) {
                            const elm = r.items[i];
                            self.success.getAllItems += "<hr/> id: " + elm.id + "<br/> list: " + elm.list + "<br/> created: " + elm.created + "<br/> modified: " + elm.modified + "<br/> data: " + elm.data + "<br/> extraData: " + elm.extraData + "<br/> status: " + elm.status;
                        }
                    }
                }
                e.target.classList.remove("loading");
            }).catch(function(error) {
                self.error.getAllItems = JSON.parse(error.request.response).Message;
                e.target.classList.remove("loading");
            });
        },
        itemExists: function(e) {
            var self = this;
            e.target.classList.add("loading");
            self.error.itemExists = "";
            self.success.itemExists = "";
            // itemExists(string context, string formName, string title, bool onlyClosed)
            axios.post(self.siteUrl + self.webMethodUrl + "itemExists", {
                formName: "UnitTest01",
                context: self.siteUrl,
                title: self.key,
                onlyClosed: false
            }, {
                "Content-type": "application/json; charset=utf-8",
            }).then(function(response) {
                var r = isJsonString(response.data.d) ? JSON.parse(response.data.d) : {
                    Message: "response error",
                };
                if (typeof r['Message'] != "undefined") {
                    self.error.itemExists = r.Message;
                } else {
                    self.success.itemExists = "Llamada completada correctamente.";
                    self.success.itemExists += "<hr/> exists: " + r.exists
                }
                e.target.classList.remove("loading");
            }).catch(function(error) {
                self.error.itemExists = JSON.parse(error.request.response).Message;
                e.target.classList.remove("loading");
            });
        },
    },
});

function isJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}
