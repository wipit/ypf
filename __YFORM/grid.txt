<link rel="stylesheet" href="./__yform/css/jsgrid.min.css" />
<link rel="stylesheet" href="./__yform/css/jsgrid-theme.min.css" />
<link rel="stylesheet" href="./__yform/css/grilla.css?v=3" />

<div id="app_grilla_datos" v-cloak>
  <div id="ContainerGrilla" class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <h2>Grilla de datos</h2>
      </div>
    </div>

    <div v-if="loading" id="topMessage" class="row">
      <div class="col-md-12">
        <div class="card-option">
          <span>{{ loadingMsg }}</span>
        </div>
      </div>
    </div>

    <div id="ContainerChecks" class="row">
      <div class="col-md-12">
        <fieldset>
          <legend>Mostrar/Ocultar columnas:</legend>
          <div class="row">
            <div class="col-md-3" v-for="data in fields">
              <input
                :id="data.name"
                type="checkbox"
                class="toppings"
                :value="data.name"
                :checked="data.visible"
              />
              <label :for="data.name">{{ data.title }}</label>
            </div>
          </div>
        </fieldset>
      </div>
    </div>

    <div id="ContainerJSGrid" class="row">
      <div class="col-md-12" id="jsGrid"></div>
    </div>

    <div class="row">
      <div class="col-md-12" style="text-align: right">
        <button class="button botonExcel" @click.prevent="exportToExcel">
          Exportar a excel
        </button>
      </div>
    </div>
  </div>
</div>

<script src="./__yform/js/axios.min.js"></script>
<script src="./__yform/js/jsgrid.min.js"></script>
<script src="./__yform/js/i18n/jsgrid-es.js"></script>
<script src="./__yform/js/xlsx.full.min.js"></script>
<script src="./__yform/js/Blob.js"></script>
<script src="./__yform/js/FileSaver.min.js"></script>
<script src="./__yform/js/grilla.js?v=3"></script>

<script type="text/javascript">
  document.addEventListener(
    "DOMContentLoaded",
    function () {
      (function loadVue() {
        if (
          typeof Vue != "undefined" &&
          typeof axios != "undefined" &&
          typeof XLSX != "undefined" &&
          typeof saveAs != "undefined" &&
          typeof GridVue != "undefined"
        ) {
          SP.SOD.executeOrDelayUntilScriptLoaded(function () {
            GridVue("#app_grilla_datos");
          }, "sp.js");
        } else {
          window.setTimeout(function () {
            loadVue();
          }, 500);
        }
      })();
    },
    true
  );
</script>
